<?php
namespace stream_bot\libs;

class TwitterStream{

	const host		= 'userstream.twitter.com';
	const full_url	= 'https://userstream.twitter.com/1.1/user.json';
	
	private $oauth;
	
	private static $connection_retry	= 5;
	private static $retry_interval		= 1000;
	
	private $retry = 0;
	
	private $callbacks = [];
	
	public function __construct(\OAuth $oauth,$callbacks = []){
		$this->oauth = $oauth;
		$this->callbacks = $callbacks;
	}
	
	private function getRequestHeader(){
		return $this->oauth->getRequestHeader('GET',self::full_url);
	}
	
	private function getRequest(){
		return 	'GET '.self::full_url.' HTTP/1.1'."\r\n"
				. 'Host: '.self::host."\r\n"
				. 'Authorization: '.$this->getRequestHeader(). "\r\n"
				. "\r\n";
	}
	

	
	public function start(){
		while(1){
			$this->conect();
			if( !$this->onRunable() || $this->retry > $this->connection_retry)break;
			$this->retry++;
			sleep($retry_interval*$this->retry);
		}
	}
	
	private function conect(){
		$fp = fsockopen('ssl://'.self::host, 443) or die("open error");
		fwrite($fp,$this->getRequest()) or die("write error");
		
		//read header
		$response	= [];
		$header		= [];
		while (($in = fgets($fp))!=false && $this->onRunable()) {
			if($in==="\r\n")break;
			elseif(preg_match("/^(.+): (.+)\r\n$/",$in,$matches)){
				$header[$matches[1]] = $matches[2];
			}elseif(preg_match("/^HTTP\/1\.1 (.+) (.*)\r\n$/",$in,$matches)){
				$response['code'] = $matches[1];
				$response['message'] = $matches[2];
			}
		}
		$this->onConection($response,$header);
		
		if($response['code'] !== '200'){
			$this->onError($response,$header);
			return;
		}
		
		$this->retry = 0;

		$in = '';
		while (($in .= fgets($fp)) != false && $this->onRunable()) {
			if(preg_match("/(.+)\r\n(.*)\r\n\r\n/",$in,$matches)){
				if( strlen($matches[2])>0 ){
					$json = json_decode($matches[2]);
					if($json){
						if( isset($json->disconnect)){
							$this->onDisconection($json);
						}else{
							$this->onJson($json);
						}
					}
				}
				$in='';
			}
		}
		fclose($fp);
		$this->onShutdown();
	}
	
	
	private function onShutdown(){
		if( isset($this->callbacks["onShutdown"]) ){
			call_user_func($this->callbacks["onShutdown"]);
		}
	}
	
	private function onConection($response,$header){
		if( isset($this->callbacks["onConection"]) ){
			call_user_func($this->callbacks["onConection"],$response,$header);
		}
	}
	
	private function onDisconection($json){
		if( isset($this->callbacks["onDisconection"]) ){
			call_user_func($this->callbacks["onDisconection"],$json);
		}
	}
	private function onRunable(){
		if( isset($this->callbacks["onRunable"]) ){
			return call_user_func($this->callbacks["onRunable"]);
		}else{
			return true;
		}
	}
	private function onJson($json){
		if( isset($this->callbacks["onJson"]) ){
			call_user_func($this->callbacks["onJson"],$json);
		}
	}
	private function onError($response,$header){
		if( isset($this->callbacks["onError"]) ){
			call_user_func($this->callbacks["onError"],$response,$header);
		}
	}
}
