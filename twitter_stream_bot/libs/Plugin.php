<?php
namespace stream_bot\libs;

use \System_Daemon;

class Plugin{
	const LOG_EMERG		= System_Daemon::LOG_EMERG;
	const LOG_ALERT		= System_Daemon::LOG_ALERT;
	const LOG_CRIT		= System_Daemon::LOG_CRIT;
	const LOG_ERR		= System_Daemon::LOG_ERR;
	const LOG_WARNING	= System_Daemon::LOG_WARNING;
	const LOG_NOTICE	= System_Daemon::LOG_NOTICE;
	const LOG_INFO		= System_Daemon::LOG_INFO;
	const LOG_DEBUG		= System_Daemon::LOG_DEBUG;
	
	private $oauth;
	
	public function setOauth($oauth){
		$this->oauth = $oauth;
	}
	
	public function name(){
		return 'plugin name';
	}
	public function version(){
		return '1.0';
	}
	
	function onSender($json){}
	
	function onStatus($json){}
	
	function onDirectMessage($json){}
	
	function onDelete($json){}
	
	function onLimit($json){}
	
	function onWarning($json){}
	
	function onScrubGeo($json){}
	
	function onFriends($json){}
	
	function onFavorite($json){}
	
	function onUnfavorite($json){}
	
	function onFollow($json){}
	
	function onUnfollow($json){}
	
	function onUserUpdate($json){}
	
	function onBlock($json){}
	
	function onUnblock($json){}
	
	function onListMemberAdded($json){}
	
	function onListMemberRemoved($json){}
	
	function onListUserSubscribed($json){}
	
	function onListUserUnsubscribed($json){}
	
	function onListCreated($json){}
	
	function onListUpdated($json){}
	
	function onListDestroyed($json){}
	
	/**
	 *	https://dev.twitter.com/docs/api/1.1/post/statuses/update
	 *	連想配列で上記URLのParametersをキーにして
	 */
	function StatusUpdate($parameters){
		if(!isset($parameters['status']))return false;
		$response = $this->post('https://api.twitter.com/1.1/statuses/update.json',$parameters);
		if($response)return json_decode($response);
		return false;
	}
	
	/**
	 * https://dev.twitter.com/docs/api/1.1/post/direct_messages/new
	 * 連想配列で上記URLのParametersをキーにして
	 */
	 function directMessagesNew($parameters){
		if(!isset($parameters['text']) || !(isset($parameters['user_id']) or isset($parameters['screen_name']) ))return false;
		$response = $this->post('https://api.twitter.com/1.1/direct_messages/new.json',$parameters);
		if($response)return json_decode($response);
		return false;
	 }
	 
	/**
	 * https://dev.twitter.com/docs/api/1.1/post/friendships/create
	 * 連想配列で上記URLのParametersをキーにして
	 */
	 function friendshipsCreate($parameters){
		if(!(isset($parameters['user_id']) or isset($parameters['screen_name']) ))return false;
		$response = $this->post('https://api.twitter.com/1.1/friendships/create.json',$parameters);
		if($response)return json_decode($response);
		return false;
	 }
	 
	/**
	 * https://dev.twitter.com/docs/api/1.1/post/friendships/destroy
	 * 連想配列で上記URLのParametersをキーにして
	 */
	 function friendshipsDestroy($parameters){
		if(!(isset($parameters['user_id']) or isset($parameters['screen_name']) ))return false;
		$response = $this->post('https://api.twitter.com/1.1/friendships/destroy.json',$parameters);
		if($response)return json_decode($response);
		return false;
	 }
	 
	/**
	 * https://dev.twitter.com/docs/api/1.1/post/account/update_profile
	 * 連想配列で上記URLのParametersをキーにして
	 */
	 function accountUpdateProfile($parameters){
 		if( isset($parameters['name']) || isset($parameters['url'])
 			|| isset($parameters['location']) || isset($parameters['description']) ){
			$response = $this->post('https://api.twitter.com/1.1/account/update_profile.json',$parameters);
			if($response)return json_decode($response);
		}
		return false;
	 }
	
	private function post($url,$parameters,&$header = ''){
		try{
			if($this->oauth->fetch($url,$parameters,'POST')){
				$response = $this->oauth->getLastResponse();
				$header = $this->oauth->getLastResponseHeaders();
				return $response;
			}
		}catch(exception $e){
			self::log(self::LOG_ERR,$e->getMessage());
		}
		return false;
	}
	
	public function log($code,$message){
		if($message instanceof Exception){
			System_Daemon::log($code, '['.$this->name().$this->version().'] '.$message->getMessage().' '.$message->getFile().':'.$message->getLine());
		}else{
			System_Daemon::log($code, '['.$this->name().$this->version().'] '.$message);
		}
	}
	
}