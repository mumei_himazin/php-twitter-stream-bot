#!/bin/sh

#get pid dir
PID_DIR=$(php /usr/local/bin/twitter_stream_bot/daemon.php -pid_dir)

#get pid
if [ -f $PID_DIR ]
then
	PID=$(cat $PID_DIR)
else
	PID=0;
fi


start(){
	if [ $PID -gt 0 ]
	then
		echo "already stated"
	else
		php /usr/local/bin/twitter_stream_bot/daemon.php
	fi
}
stop(){
	if [ $PID -gt 0 ]
	then
		echo "stoping..."
		kill -9 ${PID}
		rm ${PID_DIR}
	else
		echo "already stoped"
	fi
}

status(){
	if [ $PID -gt 0 ]
	then
		echo "running"
		echo "[pid]${PID}"
	else
		echo "not running"
	fi
}

case "$1" in
	start)
		start
		;;
	stop)
		stop
		;;
	status)
		status
		;;

esac
