#!/usr/bin/php
<?php

if(isset($argv[1]) && $argv[1]==='pid' ){
	echo file_get_contents();
}

require_once __DIR__.'/setting.php';
require_once 'System/Daemon.php';
require_once __DIR__.'/libs/autoLoader.php';

System_Daemon::setOptions($options);

if(isset($argv[1])){
	$dir = System_Daemon::opt('appPidLocation');

	if($argv[1]==='-pid'){
		if(file_exists($dir)){
			echo file_get_contents($dir),"\n";
		}else{
			echo 0;
		}
	}elseif($argv[1]==='-pid_dir'){
		echo System_Daemon::opt('appPidLocation'),"\n";
	}
	return;
}

$oauth = new \OAuth($consumer_key,$consumer_secret);
$oauth->setToken($access_token,$access_token_secret);

//load plugins
$pluginLoader = new \stream_bot\libs\PluginLoader($plugins_dir);
$pluginLoader->load($oauth);

System_Daemon::start();
echo 'running pid is ['.posix_getpid().']'."\n";



$callbacks = [
	'onRunable'		=> function(){
		return !System_Daemon::isDying();
	},
	'onConection'	=> function($response,$header){
		System_Daemon::log(System_Daemon::LOG_INFO, "onConection resonse:".$response['code'].' '.$response['message']);
	},
	'onDisconection'	=> function($response,$header){
		System_Daemon::log(System_Daemon::LOG_INFO, "onDisconection resonse:".$response['code'].' '.$response['message']);
	},
	'onError'		=> function($response,$header){
		System_Daemon::log(System_Daemon::LOG_ERR, "onError resonse:".$response['code'].' '.$response['message']);
//		System_Daemon::stop();
	},
	'onShutdown'	=> function(){
		System_Daemon::log(System_Daemon::LOG_ERR, "onShutdown");
	},
	'onJson'		=> function($json) use ($pluginLoader){
		$type = "";
		if(isset($json->sender)){
			$type = 'onSender';
		}elseif( isset($json->text)){
			$type = 'onStatus';
		}elseif( isset($json->direct_message)){
			$type = 'onDirectMessage';
		}elseif( isset($json->delete)){
			$type = 'onDelete';
		}elseif( isset($json->limit)){
			$type = 'onLimit';
		}elseif( isset($json->warning)){
			$type = 'onWarning';
		}elseif( isset($json->scrub_geo)){
			$type = 'onScrubGeo';
		}elseif( isset($json->friends)){
			$type = 'onFriends';
		}elseif( isset($json->event)){
			switch($json->event){
				case 'favorite':
					$type = 'onFavorite';
					break;
				case 'unfavorite':
					$type = 'onUnfavorite';
					break;
				case 'follow':
					$type = 'onFollow';
					break;
				case 'unfollow':
					$type = 'onUnfollow';
					break;
				case 'user_update':
					$type = 'onUserUpdate';
					break;
				case 'block':
					$type = 'onBlock';
					break;
				case 'unblock':
					$type = 'onUnblock';
					break;
				case 'list_member_added':
					$type = 'onListMemberAdded';
					break;
				case 'list_member_removed':
					$type = 'onListMemberRemoved';
					break;
				case 'list_user_subscribed':
					$type = 'onListUserSubscribed';
					break;
				case 'list_user_unsubscribed':
					$type = 'onListUserUnsubscribed';
					break;
				case 'list_created':
					$type = 'onListCreated';
					break;
				case 'list_updated':
					$type = 'onListUpdated';
					break;
				case 'list_destroyed':
					$type = 'onListDestroyed';
					break;
			}
		}
		if($type !== ""){
			foreach($pluginLoader->getPlugins() as $plugin){
				try{
					$plugin->$type($json);
				}catch(Exception $e){
					$plugin->log(System_Daemon::LOG_INFO, $e);
				}
			}
		}
		
	}
];

$stream = new \stream_bot\libs\TwitterStream($oauth,$callbacks);
$stream->start();

System_Daemon::stop();




